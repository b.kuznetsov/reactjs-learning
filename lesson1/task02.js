'use strict';

/*
 * 2. Написать функцию calculateArea, которая будет принимать параметры, 
 * для вычисления площади (можете выбрать какую то конкретную фигуру, 
 * а можете, основываясь на переданных параметрах, выполнять требуемый 
 * алгоритм вычисления площади для переданной в параметрах фигуры) и 
 * возвращать объект вида: { area, figure, input }, где area - вычисленная 
 * площадь, figure - название фигуры, для которой вычислялась площадь, 
 * input - входные параметры, по которым было произведено вычисление.
 */

// Проверка работоспособности
console.log(calculateArea("rectangle", 2, 3));
console.log(calculateArea("Circle", 35));


function calculateArea(figure = "rectangle") {
    let area;
    figure = figure.toLowerCase();
    switch(figure) {
        case "rectangle":
            checkArguments(3, arguments);
            area = calculateRectangleArea(+arguments[1], +arguments[2]);
            break;
        case "circle":
            checkArguments(2, arguments);
            area = calculateCircleArea(+arguments[1]);
            break;
        default:
            throw new Error("The figure is not supported");
    } 
    return produceResult(area, figure, arguments);
 }

 function produceResult(area, figure, args) {  
    return {
        area: area,
        figure: figure,
        input: [...args].toString()
    }; 
 }

 function checkArguments(expected = 1, args = []) {
    if (args.length < expected) {
        throw new Error(`Arguments count is less than ${expected}`);
    }
 }

 function calculateRectangleArea(width = 0, height = 0) {
    return width * height;
 }

 function calculateCircleArea(radius = 0) { 
    return Math.PI * Math.pow(radius, 2);
 }