'use strict';

/*
* 1. Написать функцию loop, которая будет принимать параметры: 
* times (значение по умолчанию = 0), callback (значение по умолчанию = null) 
* и будет в цикле (times раз), вызывать функцию callback. 
* Если функцию не передана, то цикл не должен отрабатывать ни разу. 
* Покажите применение этой функции
*/

// Проверка результата
loop(3, printHello);
loop(5);
loop(2, () => console.log('Another line'));


function loop(times = 0, callback = null) {
    console.log('-----');
    if (callback == null) {
        return;
    }
    for (let i = 0; i < times; i++) {
        callback();
    }
}

function printHello() {
    console.log('Hello, world!');
}