'use strict';

/**
 * 3. Необходимо написать иерархию классов вида:
 * Human -> Employee -> Developer
 * Human -> Employee -> Manager
 * Каждый Менеджер (Manager) должен иметь внутренний массив своих 
 * сотрудников (разработчиков), а также методы по удалению/добавлению разработчиков.
 * Каждый Разработчик (Developer) должны иметь ссылку на Менеджера и методы для 
 * изменения менеджера (имеется ввиду возможность назначить другого менеджера).
 * У класса Human должны быть следующие параметры: name (строка), age (число), 
 * dateOfBirth (строка или дата)
 * У класса Employee должны присутствовать параметры: salary (число), department (строка)
 * В классе Human должен присутствовать метод displayInfo, который возвращает 
 * строку со всеми параметрами экземпляра Human.
 * В классе Employee должен быть реализовать такой же метод (displayInfo), который 
 * вызывает базовый метод и дополняет его параметрами из экземпляра Employee
 * Чтобы вызвать метод базового класса, необходимо внутри вызова метода displayInfo 
 * класса Employee написать: super.displayInfo(), это вызовет метод disaplyInfo класс 
 * Human и вернет строку с параметрами Human'a.
 */

class Human {

    constructor(name) {
        this.name = name;
        this.age = 'N/A';
        this.dateOfBirth = 'N/A';
    }

    displayInfo() {
        let result = [];
        for (let field in this) {
            result.push(`${field}=${this[field]}`);
        }
        return result.toString();
    }

    /**
     * 2-я реализация по условию задания
     */
    displayInfo2() {
        return `name=${this.name},age=${this.age},dateOfBirth=${this.dateOfBirth}`;
    }

    toString() {
        return this.name;
    }
}

class Employee extends Human {

    constructor(name) {
        super(name);
        this.salary = 0;
        this.department = 'N/A';
    }

     /**
     * 2-я реализация по условию задания
     */
    displayInfo2() {
        return `${super.displayInfo2()},salary=${this.salary},department=${this.department}`;
    }
}

class Developer extends Employee {

    constructor(name) {
        super(name);
        this.manager = null;
    }

    assignManager(manager) {
        this.manager = manager;
    }

    resetManager() {
        this.manager = null;
    }
}

class Manager extends Developer {

    constructor(name) {
        super(name);
        this.developers = [];
    }

    addDeveloper(developer) {
        this.developers.push(developer);
        developer.assignManager(this);
    }

    removeDeveloper(developer) {
        let index = this.developers.indexOf(developer);
        if (index === -1) {
            return;
        }
        this.developers[index].resetManager();
        this.developers.splice(index, 1);
    }
}


// Проверка
var human = new Human("John", 28, "23.06.1989");
var dev = new Developer("Daniel");
var manager = new Manager("Nicolas");

console.log(human.displayInfo());
console.log(dev.displayInfo());
console.log(dev.displayInfo2());

manager.addDeveloper(dev);
console.log(manager.displayInfo());
console.log(dev.displayInfo());

manager.removeDeveloper(dev);
console.log(manager.displayInfo());
console.log(dev.displayInfo());