'use strict';

var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;

/**
 * 5*. Написать цикл, который создаёт массив промисов, внутри каждого промиса 
 * происходит обращение к ресурсу (https://jsonplaceholder.typicode.com/users/number), 
 * где вместо number подставляется число от 1 до 10, в итоге должно получиться 10 промисов. 
 * Следует дождаться выполнения загрузки всеми промисами и далее вывести массив загруженных данных.
 */

loop();

function loop() {
    const sink = [];
    const promises = []; 
    for (let i = 1; i < 11; i++) {
        promises.push(new Promise((resolve, reject) => {
            let request = new XMLHttpRequest();
            request.onload = () => {
                if (request.status === 200) {
                    let user = JSON.parse(request.responseText);
                    sink.push(user);
                    resolve(user);
                } else {
                    reject("Cannot execute request. Response: " + request.responseText);
                }
            }
            request.open('GET', getUrl(i));
            request.send();
        }));
    }
    Promise.all(promises)
        .catch(r => console.log(r))
        .then(r => console.log(JSON.stringify(sink)));
}

function getUrl(userId) {
    return `https://jsonplaceholder.typicode.com/users/${userId}`;
}