'use strict';

/**
 * 4*. При помощи генератора написать функцию - анкету, которая запрашивает у 
 * пользователя на ввод параметры и передаёт их в генератор. В конце, когда 
 * генератор завершается, он должен вернуть все введённые входные параметры в 
 * виде объекта. Этот объект нужно вывести в консоли.
 */

 function* generate() {
    let sink = []; 
    let user;
    for (let i = 0; i < 3; i++) {
        sink.push(yield user);
    }
    return console.log(JSON.stringify(sink));
 }

 function ask() {
    const generator = generate();
    generator.next();
    for (let i = 0; i < 3; i++) {
        let firstname = prompt('First name', 'John');
        let lastname = prompt('Last name', 'Smith');
        let user = {
            firstName: firstname,
            lastName: lastname
        };
        console.log(JSON.stringify(user));
        generator.next(user);
    }
}

ask();